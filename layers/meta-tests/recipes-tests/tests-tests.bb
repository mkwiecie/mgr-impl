SUMMARY = "Userspace SCT tests for drivers"
SRC_URI = "file://${TOPDIR}/sources/drivers-tests"

LICENSE = "GPL-2.0"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/GPL-2.0;md5=801f80980d171dd6425610833a22dbe6"

DEPENDS += "gtest gmock libnl"
RDEPENDS_${PN} += "libnl libnl-genl gmock tests-ks"
inherit pkgconfig

S = "${WORKDIR}${TOPDIR}/sources/drivers-tests"

do_compile() {
	oe_runmake -C framework "BASEDIR=${S}"
	oe_runmake -C tests "BASEDIR=${S}"
}

do_install() {
	install -d ${D}${bindir}
	install -m 774 ${S}/tests/build/tests ${D}${bindir}/drivers_tests
}

FILES_${PN} += "${bindir}/drivers_tests"
