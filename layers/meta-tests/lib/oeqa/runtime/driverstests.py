import subprocess
import unittest
import sys
from oeqa.oetest import oeRuntimeTest, skipModule
from oeqa.utils.decorators import *

def setUpModule():
    if not (oeRuntimeTest.hasPackage("dropbear") or oeRuntimeTest.hasPackage("openssh")):
        skipModule("No ssh package in image")

class DriverstestsTest(oeRuntimeTest):

    @testcase(301)
    @skipUnlessPassed('test_ping test_ssh')
    def test_driverstests(self):
        (status, output) = self.target.run("KERNEL_MODULES_PATH=/lib/modules/`uname -r`/ /usr/bin/drivers_tests")
        self.assertEqual(status, 0, msg="Drivers Tests failed: %s" % output)
