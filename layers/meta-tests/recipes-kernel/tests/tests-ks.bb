SUMMARY = "Kernel modules for testing drivers"
SRC_URI = "file://${TOPDIR}/sources/drivers-tests"

LICENSE = "GPL-2.0"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/GPL-2.0;md5=801f80980d171dd6425610833a22dbe6"

inherit module

S = "${WORKDIR}${TOPDIR}/sources/drivers-tests/kernel_modules"

do_install_append() {
    rm -r ${D}${base_libdir}/modules/4.4.10-yocto-standard/extra
}

FILES_kernel-modules = "${base_libdir}/modules/4.4.10-yocto-standard/module/* \
                        ${base_libdir}/modules/4.4.10-yocto-standard/drivers/* \
                       "
