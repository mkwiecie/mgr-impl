DESCRIPTION = "Image with SCTs (System Component Test) which may be run on target or virtual machine."

inherit core-image testimage

SRC_URI += "file://mdio.cfg"
LICENSE = "MIT"
IMAGE_FEATURES += "ssh-server-openssh"
IMAGE_INSTALL += "packagegroup-core-boot ${ROOTFS_PKGMANAGE_BOOTSTRAP} ${CORE_IMAGE_EXTRA_INSTALL} \
                  tests-ks tests-tests \
                 "

TEST_SUITES = "ping ssh driverstests"

IMAGE_ROOTFS_SIZE ?= "8192"
IMAGE_ROOTFS_EXTRA_SPACE_append = "${@bb.utils.contains("DISTRO_FEATURES", "systemd", " + 4096", "" ,d)}"
