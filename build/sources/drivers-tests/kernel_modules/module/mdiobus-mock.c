#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/platform_device.h>
#include <linux/phy.h>
#include <linux/err.h>
#include <linux/slab.h>

#include "communication.h"
#include "mdiobus-mock.h"

#define MODULE_NAME "MDIO bus mock"
#define LOG_PREFIX "mdiobus-mock"
#define PHY_MASK 0xfffffffe

#define RECV_TIMEOUT_MSEC 500

typedef struct {
	int phy_bus_int;
	char *short_name;
	char *long_name;
} mdio_lookup_entry_t;

static mdio_lookup_entry_t mdio_lookup_table[] = {
	{0, "0", "femac" },
};
static struct mii_bus *mock_bus;
static struct platform_device *pdev;

typedef struct {
	int user_id;
	int return_value;
	struct completion receive_completed;
} communication_client_t;

static communication_client_t test_client = { -1, -1, COMPLETION_INITIALIZER(test_client.receive_completed) };

static int nl_client_connected(void)
{
	return test_client.user_id != -1;
}

static void nl_register_cb(int user_id) {
	test_client.user_id = user_id;
	reinit_completion(&test_client.receive_completed);
}

static void nl_unregister_cb(void) {
	send_data_to_user(test_client.user_id, MDIOBUS_MOCK_ID, NULL, 0, MDIO_UNREGISTER_MSG);
	test_client.user_id = -1;
	test_client.return_value = -1;
}

static void nl_receive_result_cb(void *payload) {
	test_client.return_value = *((int*)(payload));
	complete(&test_client.receive_completed);
}

static void nl_unexpected_cb(int ptype)
{
	printk(KERN_ERR "%s: Unknown payload type!\n", LOG_PREFIX);
}

static void dispatcher(int user_id, void *payload, int payload_type) {
	switch(payload_type) {
		case MDIO_RESULT_MSG:
			nl_receive_result_cb(payload);
			break;

		case MDIO_REGISTER_MSG:
			nl_register_cb(user_id);
			break;

		case MDIO_UNREGISTER_MSG:
			nl_unregister_cb();
			break;

		default:
			nl_unexpected_cb(payload_type);
	}
}

static int receive_data_from_user(void)
{
	if (wait_for_completion_timeout(&test_client.receive_completed, msecs_to_jiffies(RECV_TIMEOUT_MSEC)) == 0) {
		printk(KERN_ERR "%s: Timeout while waiting for data from test!\n", LOG_PREFIX);
		return -1;
	}
	return test_client.return_value;
}

static int mdio_read(struct mii_bus *bus, int phy_id, int reg_num)
{
	struct mdio_read_st read_data = { phy_id, reg_num };
	if (!nl_client_connected()) {
		return 0;
	}
	send_data_to_user(test_client.user_id, MDIOBUS_MOCK_ID, (void*)&read_data, sizeof(read_data), MDIO_READ_MSG);
	return receive_data_from_user();
}

static int mdio_write(struct mii_bus *bus, int phy_id, int reg_num, u16 val)
{
	struct mdio_write_st write_data = { phy_id, reg_num, val };
	if (!nl_client_connected()) {
		return 0;
	}
	send_data_to_user(test_client.user_id, MDIOBUS_MOCK_ID, (void*)&write_data, sizeof(write_data), MDIO_WRITE_MSG);
	return receive_data_from_user();
}

static int mdio_reset(struct mii_bus *bus)
{
	if (!nl_client_connected()) {
		return 0;
	}
	send_data_to_user(test_client.user_id, MDIOBUS_MOCK_ID, NULL, 0, MDIO_RESET_MSG);
	return receive_data_from_user();
}

static void initialize_bus(void)
{
	snprintf(mock_bus->id, MII_BUS_ID_SIZE, mdio_lookup_table[0].long_name);
	mock_bus->name = MODULE_NAME;
	mock_bus->parent = &pdev->dev;
	mock_bus->read = &mdio_read;
	mock_bus->write = &mdio_write;
	mock_bus->reset = &mdio_reset;
	mock_bus->phy_mask = PHY_MASK;
}

static int initialize_kernel_module(void)
{
	int ret;

	pdev = platform_device_register_simple(MODULE_NAME, 0, NULL, 0);
	if (IS_ERR(pdev)) {
		printk(KERN_ERR "%s: Failed to register platform device!\n", LOG_PREFIX);
		return PTR_ERR(pdev);
	}

	mock_bus = mdiobus_alloc();
	if (mock_bus == NULL) {
		printk(KERN_ERR "%s: Failed to allocate memory for mdiobus!\n", LOG_PREFIX);
		platform_device_unregister(pdev);
		return -ENOMEM;
	}

	initialize_bus();

	ret = mdiobus_register(mock_bus);
	if (ret) {
		printk(KERN_ERR "%s: Failed to register mdiobus!\n", LOG_PREFIX);
		mdiobus_free(mock_bus);
		platform_device_unregister(pdev);
	} else {
		printk(KERN_INFO "%s: %s initialized successfully.\n", LOG_PREFIX, mdio_lookup_table[0].long_name);
	}

	return -ret;
}

static int __init mdio_bus_module_init(void)
{
	return initialize_kernel_module() ||
	       -register_mock(MDIOBUS_MOCK_ID, dispatcher);
}

static void __exit mdio_bus_module_exit(void)
{
	unregister_mock(MDIOBUS_MOCK_ID);
	mdiobus_unregister(mock_bus);
	mdiobus_free(mock_bus);
	platform_device_unregister(pdev);
}

module_init(mdio_bus_module_init);
module_exit(mdio_bus_module_exit);

MODULE_DESCRIPTION(MODULE_NAME);
MODULE_AUTHOR("Maciej Kwiecień");
MODULE_LICENSE("GPL");
