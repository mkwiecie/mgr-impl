#include "communication.h"
#include <linux/kernel.h>
#include <linux/module.h>
#include <net/genetlink.h>
#include <net/netlink.h>
#include <linux/netlink.h>
#include <linux/list.h>


/* family definition */
static struct genl_family com_gnl_family = {
        .id = GENL_ID_GENERATE,
        .hdrsize = 0,
        .name = "COM",
        .version = 0,
        .maxattr = COM_A_MAX,
};

static int dispatch_message(struct sk_buff *skb, struct genl_info *info);

static struct nla_policy com_gnl_policy[COM_A_MAX + 1] = {
	{ NLA_UNSPEC, 0 }, // COM_A_UNSPEC
	{ NLA_U16, 0 }, // COM_A_MOCK_ID
	{ NLA_UNSPEC, 0 }, // COM_A_PAYLOAD
	{ NLA_U32, 0 }, // COM_A_PAYLOAD_TYPE
	{ NLA_U16, 0 }, // COM_A_RESULT
};

/* operation definition */
static struct genl_ops com_gnl_ops[] = {
    {
        .cmd = COM_C_SEND_DATA,
        .flags = 0,
        .policy = com_gnl_policy,
        .doit = dispatch_message,
        .dumpit = NULL,
    },
};

struct mock_callback {
    struct list_head list;
    int mock_id;
    void (*dispatcher)(int, void*, int);
} mocks;

static struct mock_callback* find_mock(int mock_id);

static int dispatch_message(struct sk_buff *skb, struct genl_info *info) {
    struct nlattr *attr;
    int mock_id;
    int payload_type;
    void *payload;
    struct mock_callback* mock;

    if (skb == NULL) printk(KERN_WARNING "NETLINK: dispatch_message: skb jest null");
    if (info == NULL) printk(KERN_WARNING "NETLINK: dispatch_message: info jest null");

    attr = nlmsg_find_attr(info->nlhdr, sizeof(struct genlmsghdr), COM_A_MOCK_ID);
    mock_id = nla_get_u16(attr);
    attr = nlmsg_find_attr(info->nlhdr, sizeof(struct genlmsghdr), COM_A_PAYLOAD);
    payload = nla_data(attr);
    attr = nlmsg_find_attr(info->nlhdr, sizeof(struct genlmsghdr), COM_A_PAYLOAD_TYPE);
    payload_type = nla_get_u32(attr);

    mock = find_mock(mock_id);

    if (mock != NULL) {
        mock->dispatcher(info->snd_portid, payload, payload_type);
    } else {
        printk(KERN_ERR "NETLINK: Mock with given ID = %d is not found.\n", mock_id);
        return COM_RESULT_MOCK_NOT_EXIST;
    }
    return COM_RESULT_OK;
}

static struct mock_callback* find_mock(int mock_id) {
    struct mock_callback *tmp = NULL;
    struct list_head *pos;
    list_for_each(pos, &mocks.list) {
        tmp = list_entry(pos, struct mock_callback, list);
        if (tmp->mock_id == mock_id) {
            return tmp;
        }
    }
    return NULL;
}

static void delete_all_mocks(void) {
    struct list_head *pos, *q;
    struct mock_callback *tmp;

    list_for_each_safe(pos, q, &mocks.list){
        tmp = list_entry(pos, struct mock_callback, list);
        list_del(pos);
        kfree(tmp);
    }
}

int send_data_to_user(int user_id, int mock_id, void* payload, int payload_size, int payload_type) {
    struct sk_buff *skb;
    int rc;
    void *msg_head;
    const int seq = 1;

    skb = genlmsg_new(NLMSG_GOODSIZE, GFP_KERNEL);
    if (skb == NULL) {
        printk(KERN_INFO "NETLINK: send_to_user: genlmsg_new failed\n");
        return -1;
    }

    msg_head = genlmsg_put(skb, 0, seq, &com_gnl_family, 0, COM_C_SEND_DATA);
    if (msg_head == NULL) {
        printk(KERN_INFO "NETLINK: send_to_user: genlmsg_put failed\n");
        rc = -ENOMEM;
        return -1;
    }

    rc = nla_put_u16(skb, COM_A_MOCK_ID, mock_id);
    if (rc != 0) {
        printk(KERN_INFO "NETLINK: send_to_user: nla_put_u32 with COM_A_MOCK_ID failed\n");
        return -1;
    }

    rc = nla_put(skb, COM_A_PAYLOAD, payload_size, payload);
    if (rc != 0) {
        printk(KERN_INFO "NETLINK: send_to_user: nla_put_u32 with COM_A_MOCK_ID failed\n");
        return -1;
    }

    rc = nla_put_u32(skb, COM_A_PAYLOAD_TYPE, payload_type);
    if (rc != 0) {
        printk(KERN_INFO "NETLINK: send_to_user: nla_put_u32 with COM_A_MOCK_ID failed\n");
        return -1;
    }
    /* finalize the message */
    genlmsg_end(skb, msg_head);

    rc = genlmsg_unicast(&init_net, skb, user_id);
    if (rc != 0) {
        printk(KERN_INFO "NETLINK: send_to_user: genlmsg_unicast failed with %d error code\n", rc);
        return -1;
    }
    return COM_RESULT_OK;
}
EXPORT_SYMBOL(send_data_to_user);

int register_mock(int mock_id, void (*dispatcher)(int, void*, int)) {
    struct mock_callback *tmp = NULL;

    struct list_head *pos;
    list_for_each(pos, &mocks.list) {
        tmp = list_entry(pos, struct mock_callback, list);
        if (tmp->mock_id == mock_id) {
            printk(KERN_WARNING "NETLINK: There is already a mock registered with id: %d\n", mock_id);
            return COM_RESULT_MOCK_ALREADY_EXIST;
        }
    }

    tmp = (struct mock_callback*)kmalloc(sizeof(struct mock_callback), GFP_KERNEL);
    tmp->mock_id = mock_id;
    tmp->dispatcher = dispatcher;

    if (tmp == NULL) {
        printk(KERN_ERR "NETLINK: Failed to allocate memory for module registration.\n");
        return COM_RESULT_MEM_ALLOC_FAILED;
    }
    list_add(&(tmp->list), &(mocks.list));
    printk(KERN_INFO "NETLINK: Mock with id: %d registered.\n", mock_id);
    return COM_RESULT_OK;
}
EXPORT_SYMBOL(register_mock);

int unregister_mock(int mock_id) {
    struct list_head *pos, *q;
    struct mock_callback *tmp;

    list_for_each_safe(pos, q, &mocks.list){
        tmp = list_entry(pos, struct mock_callback, list);
        if (tmp->mock_id == mock_id) {
            list_del(pos);
            kfree(tmp);
            return 0;
        }
    }
    return -1;
}
EXPORT_SYMBOL(unregister_mock);

static int __init netlink_init(void){
    int rc;

    rc = _genl_register_family_with_ops_grps(&com_gnl_family,
            com_gnl_ops, COM_C_MAX, NULL, 0);
    if (rc != 0) {
        return -1;
    }

    INIT_LIST_HEAD(&mocks.list);
    printk(KERN_INFO "NETLINK: netlink_init(void): sucessful\n");
    return 0;
}

static void __exit netlink_exit(void){
    genl_unregister_family(&com_gnl_family);
    delete_all_mocks();
    printk(KERN_INFO "NETLINK: Module unloaded.\n");
}

module_init(netlink_init);
module_exit(netlink_exit);

MODULE_AUTHOR("Maciej Kwiecień");
MODULE_LICENSE("GPL");
