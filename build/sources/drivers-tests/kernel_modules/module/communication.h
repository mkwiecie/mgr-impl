#ifndef COMMUNICATION_H_
#define COMMUNICATION_H_

#if defined(__KERNEL__)
int register_mock(int mock_id, void (*dispatcher)(int, void*, int));
int send_data_to_user(int user_id, int mock_id, void* payload, int payload_size, int payload_type);
int unregister_mock(int mock_id);
#endif

// Return codes
enum {
	COM_RESULT_OK = 0,
	COM_RESULT_MOCK_NOT_EXIST,
	COM_RESULT_MOCK_ALREADY_EXIST,
	COM_RESULT_MEM_ALLOC_FAILED,
};

/* attributes */
enum {
	COM_A_UNSPEC,
	COM_A_MOCK_ID,
	COM_A_PAYLOAD,
	COM_A_PAYLOAD_TYPE,
	COM_A_RESULT,
	__COM_A_MAX,
};
#define COM_A_MAX (__COM_A_MAX - 1)

/* commands */
enum {
	COM_C_UNSPEC,
	COM_C_SEND_DATA,
	__COM_C_MAX,
};
#define COM_C_MAX (__COM_C_MAX - 1)

typedef enum mock_ids {
	MDIOBUS_MOCK_ID = 1,
	I2C_MOCK_ID = 2,
} mock_ids;

#endif /* COMMUNICATION_H_ */
