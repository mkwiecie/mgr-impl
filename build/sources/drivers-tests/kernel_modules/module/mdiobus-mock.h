#ifndef MDIOBUS_MOCK_H_
#define MDIOBUS_MOCK_H_

#include "communication.h"

enum payload_types {
	MDIO_READ_MSG,
	MDIO_WRITE_MSG,
	MDIO_RESET_MSG,
	MDIO_RESULT_MSG,
	MDIO_REGISTER_MSG,
	MDIO_UNREGISTER_MSG,
};

struct mdio_read_st
{
	int phy_id;
	int reg_num;
};

struct mdio_write_st
{
	int phy_id;
	int reg_num;
	unsigned int val;
};

#endif /* MDIOBUS_MOCK_H_ */
