#include <linux/kernel.h>
#include <linux/device.h>
#include <linux/module.h>
#include <linux/phy.h>
#include <linux/netdevice.h>
#include <linux/errno.h>

#define DRIVERNAME "mdio_access"
#define DRIVER_VERSION "1.0"

#define PHY_MAX_ADDRESS 0xff
#define PHY_MAX_REG_ADDR 0xff
#define PHY_MAX_REG_VAL 0xffff

#define MDIO_PAGE_PHYREG 0x16
#define MAX_MDIO_CACHE 16

static struct device mdio_device;
static struct class *mdio_bus_class = NULL;
static int mdiobus_cache_size = 0;
static struct mii_bus *mdiobus_cache[MAX_MDIO_CACHE];

static int mdio_cmd_output = 0;

typedef struct {
        int phy_bus_int;
        char *short_name;
        char *long_name;
} mdio_lookup_entry_t;

static mdio_lookup_entry_t mdio_access_lookup_table[] = {
        {0, "0", "femac" },
};

static void get_long_name_if_defined_for_bus(char *short_name, char *long_name)
{
    int i;
    for (i = 0; i < ARRAY_SIZE(mdio_access_lookup_table); i++) {
        if (!strcmp(mdio_access_lookup_table[i].short_name, short_name))
            strcpy(long_name, mdio_access_lookup_table[i].long_name);
    }
}

static int get_mdio_class(struct device *dev, void *data)
{
	struct phy_device *phydev;
	phydev = to_phy_device(dev);
	if (phydev) {
		mdio_bus_class = phydev->bus->dev.class;
		return 1;
	}
	return 0;
}

static inline void initialize_mdio_class(void)
{
	bus_for_each_dev(&mdio_bus_type, NULL, NULL, get_mdio_class);
}

static int mdio_bus_match_id(struct device *dev, const void *mii_id)
{
    struct mii_bus *mii = to_mii_bus(dev);

    if (!strcmp(mii->id, (const char*)mii_id))
        return 1;
    return 0;
}

static struct mii_bus *get_mii_bus_by_name(const char *name)
{
    struct device *dev;
    struct mii_bus *mdiobus;
    int i;

    if (mdiobus_cache_size > MAX_MDIO_CACHE) {
        pr_info("%s: this is very unlikely to happen!\n", __func__);
        return NULL;
    }
    for (i = 0; i < mdiobus_cache_size; i++) {
        if (!strcmp(name, mdiobus_cache[i]->id)) {
            return mdiobus_cache[i];
        }
    }

    dev = class_find_device(mdio_bus_class, NULL, name, mdio_bus_match_id);
    if (dev)
        mdiobus = to_mii_bus(dev);
    else
        return NULL;

    if (mdiobus_cache_size < MAX_MDIO_CACHE) {
        mdiobus_cache[mdiobus_cache_size++] = mdiobus;
    } else {
        pr_err("%s: mdiobus_cache is full, size %d/%d\n", __func__, mdiobus_cache_size, MAX_MDIO_CACHE);
        for (i = 0; i < mdiobus_cache_size; i++) {
            pr_err("%s: mdiobus_cache[%d] -> <%s>\n", __func__, i, mdiobus_cache[i]->id);
        }
    }

    return mdiobus;
}


int mdio_access_read(const char *bus_id, int phy_id, int reg_num, u16 *dst)
{
    struct mii_bus *mdiobus;
    int ret;

    mdiobus = get_mii_bus_by_name(bus_id);
    if (!mdiobus) {
        pr_err("%s: no such bus <%s>\n", __func__, bus_id);
        return -ENODEV;
    }

    ret = mdiobus->read(mdiobus, phy_id, reg_num);
    *dst = ret;

    return (ret < 0) ? ret : 0;
}

int mdio_access_write(const char *bus_id, int phy_id, int reg_num, u16 value)
{
    struct mii_bus *mdiobus;
    int err;

    pr_debug("%s: Write: phy: %s:%x, reg: %x, value: 0x%x\n",
            __func__, bus_id, phy_id, reg_num, value);

    mdiobus = get_mii_bus_by_name(bus_id);
    if (!mdiobus) {
        pr_err("%s: no such bus <%s>\n", __func__, bus_id);
        return -ENODEV;
    }

    pr_debug("%s: writing back to the phy 0x%x\n", __func__, value);
    err = mdiobus->write(mdiobus, phy_id, reg_num, value);

    return err;
}

static ssize_t mdio_cmd_store(struct device *dev,
        struct device_attribute *attr,
        const char *buf, size_t count)
{
    unsigned int phy_id;
    unsigned int reg_num;
    unsigned int value = 0;
    char bus_id[MII_BUS_ID_SIZE + 3];
    int err;
    char long_name[MII_BUS_ID_SIZE] = "\0";

    err = sscanf(buf, "%s %X %X %X", bus_id, &phy_id, &reg_num, &value);

    get_long_name_if_defined_for_bus(bus_id, long_name);
    if (strlen(long_name) > 1)
        strcpy(bus_id, long_name);

    pr_debug("%s: args recognised = %d, bus = <%s>, phy_id = 0x%x, reg_num = 0x%x\n",
        __func__, err, bus_id, phy_id, reg_num);

    /* Check format of everything given and alert if something is not right */
    if (err != 3 && err != 4) {
        pr_debug("%s: Invalid format given <%s>\n", __func__, buf);
        return -EINVAL;
    } else if (phy_id > PHY_MAX_ADDRESS) {
        pr_debug("%s: Invalid phy address (0x%X)\n", __func__, phy_id);
        return -EINVAL;
    } else if (reg_num > PHY_MAX_REG_ADDR) {
        pr_debug("%s: Invalid register address (0x%X)\n", __func__, reg_num);
        return -EINVAL;
    } else if (((unsigned int)value) > PHY_MAX_REG_VAL) {
        pr_debug("%s: Invalid register value (0x%X)\n", __func__, value);
        return -EINVAL;
    }

    /*
     * Command is distinguished based on number of params given:
     *  3 - read phy
     *  4 - write to phy
     * */
    if (err == 3) {
        u16 tmp;
        int ret;

        pr_debug("%s: Read: phy: %s:%x, reg: 0x%X -> 0x%X\n",
                __func__, bus_id, phy_id, reg_num, mdio_cmd_output);
        ret = mdio_access_read(bus_id, phy_id, reg_num, &tmp);
        if (ret < 0) {
            pr_err("%s: phy_read failed: 0x%X:0x%X\n", __func__, phy_id, reg_num);
            return ret;
        }
        mdio_cmd_output = tmp;
    } else if (err == 4) {
        pr_debug("%s: Write: phy: %s:%x, reg: 0x%X <- 0x%X \n",
                __func__, bus_id, phy_id, reg_num, value);
        err = mdio_access_write(bus_id, phy_id, reg_num, value);
        if (err) {
            pr_err("%s: phy_write failed: 0x%X:0x%X <- 0x%X>\n",
                       __func__, phy_id, reg_num, value);
                return -EIO;
        }
    } else {
        /* We should not be here by any chance */
        pr_err("%s Unreacheable code reached\n", __func__);
    }

    return strlen(buf)+1;
}

static ssize_t mdio_cmd_show(struct device *dev, struct device_attribute *attr, char *buf)
{
    pr_debug("%s\n", __func__);
    return snprintf(buf, PAGE_SIZE - 1, "0x%04X\n", mdio_cmd_output)+1;
}

static DEVICE_ATTR(mdio_cmd, S_IRUSR|S_IRGRP|S_IWUSR|S_IWGRP, mdio_cmd_show, mdio_cmd_store);

static struct attribute *mdio_attr[] = {
    &dev_attr_mdio_cmd.attr,
    NULL
};
static struct attribute_group mdio_attr_grp = {
    .attrs = mdio_attr
};

static void mdio_dev_release(struct device *device)
{
    pr_debug("%s\n", __func__);
}

static struct device_type mdio_dev_type = {
    .name = "mdio_device",
    .release = mdio_dev_release,
};

static int __init mdio_access_init(void)
{
    int err;

    mdio_device.type = &mdio_dev_type;
    dev_set_name(&mdio_device, "mdio_access");

    err = device_register(&mdio_device);
    if (err) {
        printk(KERN_ERR "%s: Failed to register mdio device\n", __func__);
        goto exit;
    }

    err = sysfs_create_group(&mdio_device.kobj, &mdio_attr_grp);
    if (err) {
        pr_err("%s: Failed to add sysfs\n", __func__);
        goto sysfs_failed;
    }

    initialize_mdio_class();

    pr_info(DRIVERNAME " version " DRIVER_VERSION " loaded\n");

    return 0;

sysfs_failed:
    device_unregister(&mdio_device);
exit:
    pr_err("%s: mdio_access loading failed\n", __func__);
    return -EIO;
}

static void __exit mdio_access_exit(void)
{
    int i;
    /* drop refs to all mdiobuses cached */
    for (i = 0; i < mdiobus_cache_size; i++) {
        put_device(&(mdiobus_cache[i]->dev));
        mdiobus_cache[i] = NULL;
    }
    mdiobus_cache_size = MAX_MDIO_CACHE+1;
    device_unregister(&mdio_device);
    pr_info(DRIVERNAME " version " DRIVER_VERSION " unloaded\n");
}

module_init(mdio_access_init);
module_exit(mdio_access_exit);

MODULE_AUTHOR("Maciej Kwiecień");
MODULE_LICENSE("GPL");
