/*
 * mdio_access_interface.h
 *
 *  Created on: Jul 1, 2016
 *      Author: Maciej Kwiecień
 */

#ifndef MDIO_ACCESS_INTERFACE_H_
#define MDIO_ACCESS_INTERFACE_H_

#include <string>

class mdio_access_interface
{
public:
    mdio_access_interface();
    int mdio_read(char *bus_id, int phy_id, int reg_num, unsigned int *value);
    int mdio_write(char *bus_id, int phy_id, int reg_num, unsigned int value);

private:
    int write_to_file(char* bus_id, int phy_id, int reg_num, unsigned int value);
    int write_to_file(char* bus_id, int phy_id, int reg_num);
    int read_from_file(unsigned int* value);

private:
    const std::string mdio_access_filename;
};


#endif /* MDIO_ACCESS_INTERFACE_H_ */
