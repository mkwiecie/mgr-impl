/*
 * mdio_access_interface.cc
 *
 *  Created on: Jul 1, 2016
 *      Author: Maciej Kwiecień
 */

#include "mdio_access_interface.h"
#include <fstream>
#include <cstdlib>
#include <cstring>
#include <cerrno>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

#define PHY_COMMAND_MAX_LENGTH 64

mdio_access_interface::mdio_access_interface() : mdio_access_filename("/sys/devices/mdio_access/mdio_cmd") {}

int mdio_access_interface::mdio_read(char* bus_id, int phy_id, int reg_num, unsigned int* value)
{
	int status = 0;
	status = write_to_file(bus_id, phy_id, reg_num);
	if (status < 0) {
		return status;
	}
	status = read_from_file(value);
	if (status < 0) {
		return status;
	}
	return 0;
}

int mdio_access_interface::mdio_write(char* bus_id, int phy_id, int reg_num, unsigned int value)
{
	return write_to_file(bus_id, phy_id, reg_num, value);
}

int mdio_access_interface::write_to_file(char* bus_id, int phy_id, int reg_num, unsigned int value)
{
	int fd;
	char mii_io[PHY_COMMAND_MAX_LENGTH];

	snprintf(mii_io, PHY_COMMAND_MAX_LENGTH-1, "%s %X %X %X\n", bus_id, phy_id, reg_num&0xff, value&0xffff);
	
	fd = open(mdio_access_filename.c_str(), O_RDWR);
	if(fd < 0) {
		return -errno;
	}

	if(write(fd, mii_io, strlen(mii_io)) < 0) { 
		close(fd);
		return -errno;
	}

	if (close(fd)) {
		return -errno;
	}

	return 0;
}

int mdio_access_interface::write_to_file(char* bus_id, int phy_id, int reg_num)
{
	int fd;
	char mii_io[PHY_COMMAND_MAX_LENGTH];

	snprintf(mii_io, PHY_COMMAND_MAX_LENGTH-1, "%s %X %X\n", bus_id, phy_id, reg_num&0xff);
	
	fd = open(mdio_access_filename.c_str(), O_RDWR);
	if(fd < 0) {
		return -errno;
	}

	if(write(fd, mii_io, strlen(mii_io)) < 0) { 
		close(fd);
		return -errno;
	}

	if (close(fd)) {
		return -errno;
	}

	return 0;
}

int mdio_access_interface::read_from_file(unsigned int* value)
{
    std::ifstream file(mdio_access_filename.c_str());
	if (file.good()) {
		file >> std::hex >> *value;
		return file.good() ? (0 || -errno) : -1;
	}
    return -1;
}

