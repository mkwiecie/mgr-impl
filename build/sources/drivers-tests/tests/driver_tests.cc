/*
 * driver_tests.cc
 *
 *  Created on: Feb 22, 2016
 *      Author: Maciej Kwiecień
 */

#include <gtest/gtest.h>
#include <kernel_module_loader.h>
#include <mdio_access_interface.h>
#include "mocks/mdiobus_usermock.h"

namespace consts {
static const std::string driver_filename = "drivers/mdio_driver.ko";
static const std::string mdiobus_mock_filename = "module/mdiobus-mock.ko";
static const std::string communication_filename = "module/communication.ko";
} // namespace consts

#define ERROR -1
#define SUCCESS 0

using namespace ::testing;
class driver_tests : public Test {
public:
	static void SetUpTestCase()
	{
		EXPECT_TRUE(loader.load({
		    consts::communication_filename,
		    consts::mdiobus_mock_filename,
		    consts::driver_filename}));
	}

	static void TearDownTestCase()
	{
	    EXPECT_TRUE(loader.unload({
	        consts::driver_filename,
	        consts::mdiobus_mock_filename,
	        consts::communication_filename}));
	}

	driver_tests()
	{
	    mdiobus_mock.connect();
	}

	~driver_tests()
	{
	    mdiobus_mock.disconnect();
	}

protected:
	static kernel_module_loader loader;
	mdiobus_usermock mdiobus_mock;
	mdio_access_interface mdio_us_if;
};
kernel_module_loader driver_tests::loader;

TEST_F(driver_tests, mdio_read)
{
	unsigned int value;
	char bus_id[] = {'0', '\0'};
	const int phy_id = 1;
	const int reg_num = 87;

	EXPECT_CALL(mdiobus_mock, mdiobus_read(phy_id, reg_num))
	        .WillOnce(Return(0x0BED));

	ASSERT_EQ(SUCCESS, mdio_us_if.mdio_read(bus_id, phy_id, reg_num, &value));
	EXPECT_EQ(0x0BED, value);
}

TEST_F(driver_tests, mdio_write)
{
	char bus_id[] = {'0', '\0'};
	const int phy_id = 1;
	const int reg_num = 87;

	EXPECT_CALL(mdiobus_mock, mdiobus_write(phy_id, reg_num, 0xDEAD))
	        .WillOnce(Return(SUCCESS));

	ASSERT_EQ(SUCCESS, mdio_us_if.mdio_write(bus_id, phy_id, reg_num, 0xDEAD));
}

TEST_F(driver_tests, mdio_write_nok)
{
	char bus_id[] = {'0', '\0'};
	const int phy_id = 1;
	const int reg_num = 87;

	EXPECT_CALL(mdiobus_mock, mdiobus_write(phy_id, reg_num, 0xDEAD))
	        .WillOnce(Return(ERROR));

	ASSERT_EQ(-EIO, mdio_us_if.mdio_write(bus_id, phy_id, reg_num, 0xDEAD));
}

