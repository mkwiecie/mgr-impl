/*
 * mdiobus_mock_controller.h
 *
 *  Created on: May 20, 2016
 *      Author: Maciej Kwiecień
 */

#ifndef MDIOBUS_MOCK_CONTROLLER_H
#define MDIOBUS_MOCK_CONTROLLER_H

#include <mdiobus-mock.h>
#include <mock_controller.h>
#include <map>
#include <functional>

class mdiobus_mock_controller : public mock_controller
{
public:
    mdiobus_mock_controller();
    virtual ~mdiobus_mock_controller();

    virtual int mdiobus_read(int phy_id, int reg_num) = 0;
    virtual int mdiobus_write(int phy_id, int reg_num, unsigned int value) = 0;
    virtual int mdiobus_reset() = 0;

private:
    void dispatcher(void *payload, int payload_size, int payload_type);
    void read_handler(void *payload, size_t payload_size);
    void write_handler(void *payload, size_t payload_size);
    void reset_handler(void *payload, size_t payload_size);

    const std::map<payload_types, std::function<void(void*, size_t)>> handlers;
};

#endif /* MDIOBUS_MOCK_CONTROLLER_H */
