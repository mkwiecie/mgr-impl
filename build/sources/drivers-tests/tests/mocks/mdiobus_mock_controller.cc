/*
 * mdiobus_mock_controller.cpp
 *
 *  Created on: May 20, 2016
 *      Author: Maciej Kwiecień
 */

#include <gtest/gtest.h>
#include "mdiobus_mock_controller.h"

namespace ph = std::placeholders;
mdiobus_mock_controller::mdiobus_mock_controller()
    : mock_controller(common_mock_data{MDIOBUS_MOCK_ID, MDIO_REGISTER_MSG, MDIO_UNREGISTER_MSG}),
      handlers {
          {MDIO_READ_MSG, std::bind(&mdiobus_mock_controller::read_handler, this, ph::_1, ph::_2)},
          {MDIO_WRITE_MSG, std::bind(&mdiobus_mock_controller::write_handler, this, ph::_1, ph::_2)},
          {MDIO_RESET_MSG, std::bind(&mdiobus_mock_controller::reset_handler, this, ph::_1, ph::_2)},
          {MDIO_UNREGISTER_MSG, [] (void *, size_t) { }}
      }
{}

mdiobus_mock_controller::~mdiobus_mock_controller()
{}

void mdiobus_mock_controller::dispatcher(void *payload, int payload_size, int payload_type)
{
    auto handler = handlers.find(static_cast<payload_types>(payload_type));
    if (handler == handlers.end()) {
        ASSERT_FALSE(true);
    }
    handler->second(payload, payload_size);
}

void mdiobus_mock_controller::read_handler(void *payload, size_t payload_size)
{
    const mdio_read_st *data_from_mock = receive<mdio_read_st>(payload, payload_size);
    ASSERT_TRUE(data_from_mock);
    int ret = mdiobus_read(data_from_mock->phy_id, data_from_mock->reg_num);
    send(&ret, sizeof(ret), MDIO_RESULT_MSG);
}

void mdiobus_mock_controller::write_handler(void *payload, size_t payload_size)
{
    const mdio_write_st *data_from_mock = receive<mdio_write_st>(payload, payload_size);
    ASSERT_TRUE(data_from_mock);
    int ret = mdiobus_write(data_from_mock->phy_id, data_from_mock->reg_num, data_from_mock->val);
    send(&ret, sizeof(ret), MDIO_RESULT_MSG);
}

void mdiobus_mock_controller::reset_handler(void *payload, size_t payload_size)
{
    ASSERT_EQ(0u, payload_size);
    ASSERT_EQ(nullptr, payload);
    int ret = mdiobus_reset();
    send(&ret, sizeof(ret), MDIO_RESULT_MSG);
}
