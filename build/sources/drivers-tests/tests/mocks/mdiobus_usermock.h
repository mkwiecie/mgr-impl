/*
 * mdiobus_usermock.h
 *
 *  Created on: May 20, 2016
 *      Author: Maciej Kwiecień
 */

#ifndef MDIOBUS_USERMOCK_H
#define MDIOBUS_USERMOCK_H

#include <gmock/gmock.h>
#include "mdiobus_mock_controller.h"

class mdiobus_usermock : public mdiobus_mock_controller
{
public:
    MOCK_METHOD2(mdiobus_read, int(int phy_id, int reg_num));
    MOCK_METHOD3(mdiobus_write, int(int phy_id, int reg_num, unsigned int value));
    MOCK_METHOD0(mdiobus_reset, int());
};

#endif // MDIOBUS_USERMOCK_H
