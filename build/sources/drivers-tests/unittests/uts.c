#include <stdio.h>
#include <stdarg.h>
#include <linux/phy.h>

struct class;
struct _ddebug;
struct kobject;
struct attribute_group;
struct device;

struct bus_type mdio_bus_type; //referenced in phy.h
struct device* dev_ptr;        //should be set in test

int printk(const char *fmt, ...)
{
	//int out;
	//va_list args;
	//va_start(args, fmt);
	//out = vprintf(fmt, args);
	//va_end(args);
	return 0;
}

struct device *class_find_device(struct class *class, struct device *start, const void *data, int (*match)(struct device *, const void *))
{
	return dev_ptr;
}

void __dynamic_pr_debug(struct _ddebug* not_used, const char *fmt, ...)
{
	(void*)(not_used);
	va_list args;
	va_start(args, fmt);
	vprintf(fmt, args);
	va_end(args);
}

int dev_set_name(struct device *dev, const char *fmt, ...)
{
	(void*)(dev);
	(void*)(fmt);
	return 0;
}

int device_register(struct device *dev)
{
	(void*)(dev);
	return 0;
}

int sysfs_create_group(struct kobject *kobj, const struct attribute_group *grp)
{
	(void*)(kobj);
	(void*)(grp);
	return 0;
}

int bus_for_each_dev(struct bus_type *bus, struct device *start, void *data, int (*fn)(struct device *, void *))
{
	(void*)(bus);
	(void*)(start);
	(void*)(data);
	(void*)(fn);
	return 0;
}

void put_device(struct device *dev)
{
	(void*)(dev);
}

void device_unregister(struct device *dev)
{
	(void*)(dev);
}


extern int mdio_access_read(const char *bus_id, int phy_id, int reg_num, u16 *dst);
extern int mdio_access_write(const char *bus_id, int phy_id, int reg_num, u16 value);

int read_return = 0;
int write_return = 0;
int written_value = 0;

int phy_id = 1;
int reg_num = 57;
struct mii_bus mock_bus;

int read_mock(struct mii_bus* bus, int phy, int reg)
{
	if (reg == reg_num)
		printf("reg_num ok! ");
	else
		printf("reg_num nok! ");
	if (phy == phy_id)
		printf("phy_id ok! ");
	else
		printf("phy_id nok! ");
	if (&mock_bus == bus)
		printf("bus ok! ");
	else
		printf("bus nok! ");
	return read_return;
}

int write_mock(struct mii_bus* bus, int phy, int reg, u16 value)
{
	if (reg == reg_num)
		printf("reg_num ok! ");
	else
		printf("reg_num nok! ");
	if (phy == phy_id)
		printf("phy_id ok! ");
	else
		printf("phy_id nok! ");
	if (&mock_bus == bus)
		printf("bus ok! ");
	else
		printf("bus nok! ");
	written_value = value;
	return write_return;
}

int main()
{
	printf("Start unittests");
	char bus[] = {'0', '\0'};
	u16 write_value = 0;
	u16 read_value = 0;
	int status = 0;
	int overall_score = 0;

	mock_bus.read = read_mock;
	mock_bus.write = write_mock;
	dev_ptr = &(mock_bus.dev);

	read_return = 999;
	printf("\nread_ok testcase: ");
	status = mdio_access_read(bus, phy_id, reg_num, &read_value);
	if (!(status == 0 && read_value == read_return)) {
		printf("FAILED!");
		overall_score--;
	} else {
		printf("PASSED");
	}

	write_return = 0;
	write_value = 16;
	printf("\nwrite_ok testcase: ");
	status = mdio_access_write(bus, phy_id, reg_num, write_value);
	if (!(status == write_return && written_value == write_value)) {
		printf("FAILED!");
		overall_score--;
	} else {
		printf("PASSED");
	}
	
	read_return = -1;
	printf("\nread_nok testcase: ");
	status = mdio_access_read(bus, phy_id, reg_num, &read_value);
	if (!(status == read_return && read_value == (u16)(-1))) {
		printf("FAILED!");
		overall_score--;
	} else {
		printf("PASSED");
	}

	write_return = -1;
	printf("\nwrite_nok testcase: ");
	status = mdio_access_write(bus, phy_id, reg_num, write_value);
	if (!(status == write_return)) {
		printf("FAILED!");
		overall_score--;
	} else {
		printf("PASSED");
	}

	write_return = 0;
	char bad_bus[] = { '1', '\0'};
	dev_ptr = NULL;
	printf("\nbad_bus testcase: ");
	status = mdio_access_write(bad_bus, phy_id, reg_num, write_value);
	if (!(status == -ENODEV)) {
		printf("FAILED!");
		overall_score--;
	} else {
		printf("PASSED");
	}

	printf("\n");

	return overall_score;
}

