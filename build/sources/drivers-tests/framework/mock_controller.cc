/*
 * mock_controller_base.cpp
 *
 *  Created on: May 20, 2016
 *      Author: Maciej Kwiecień
 */

#include <gtest/gtest.h>
#include "mock_controller.h"

mock_controller::mock_controller(const common_mock_data &mock_info)
    : mock_info(mock_info), should_receive(true)
{}

mock_controller::~mock_controller()
{}

void mock_controller::send(void *payload, int payload_size, int payload_type)
{
    nl_socket.send(mock_info.mock_id, payload, payload_size, payload_type);
}

void mock_controller::connect()
{
    namespace ph = std::placeholders;
    nl_socket.register_handler(mock_info.mock_id,
                               std::bind(&mock_controller::dispatcher, this, ph::_1, ph::_2, ph::_3));
    receiver_thread.reset(new std::thread(std::bind(&mock_controller::receiver, this)));
    register_in_kernelspace();
}

void mock_controller::disconnect()
{
    should_receive = false;
    unregister_from_kernelspace();
    receiver_thread->join();
    nl_socket.unregister_handler(mock_info.mock_id);
}

void mock_controller::register_in_kernelspace()
{
    nl_socket.send(mock_info.mock_id, NULL, 0, mock_info.register_msg_id);
}

void mock_controller::unregister_from_kernelspace()
{
    nl_socket.send(mock_info.mock_id, NULL, 0, mock_info.unregister_msg_id);
}

void mock_controller::receiver()
{
    while (should_receive) {
        nl_socket.receive();
    }
}

