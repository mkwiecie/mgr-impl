#ifndef NETLINK_COMMUNICATION_H
#define NETLINK_COMMUNICATION_H

#include <cstdint>
#include <functional>
#include <vector>
#include <map>
#include <mutex>

#include <netlink/netlink.h>
#include <netlink/socket.h>
#include <netlink/genl/genl.h>
#include <linux/genetlink.h>
#include <netlink/genl/mngt.h>

using handler_prototype = std::function<void (void *payload, int payload_size, int payload_type)>;
using mock_id = int;

class NLSocket
{
public:
	NLSocket();
	~NLSocket();

	void receive() const;
	void send(mock_id id, void *payload, int payload_size, int payload_type);
	int register_handler(mock_id id, handler_prototype handler);
	int unregister_handler(mock_id id);

private:
	int register_family();
	static int parser(struct nl_cache_ops *cache, struct genl_cmd *cmds, struct genl_info *info, void *arg);
	static int result_parser(struct nl_cache_ops *cache, struct genl_cmd *cmds, struct genl_info *info, void *arg);

private:
	using lock_t = std::unique_lock<std::mutex>;

	std::vector<struct genl_cmd> cmds;
	struct genl_ops ops;
	int family;
	std::map<mock_id, handler_prototype> handlers;
	std::mutex handlers_mutex;
	nl_sock *socket_;
};

#endif // NETLINK_COMMUNICATION_H
