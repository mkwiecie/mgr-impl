#include <iostream>
#include <string>
#include <netlink/attr.h>
#include <communication.h>
#include "netlink_communication.h"

static std::string com_c_send_data_name = "COM_C_SEND_DATA";
static std::string com_name = "COM";

static struct nla_policy com_gnl_policy[COM_A_MAX + 1] = {
	{ NLA_UNSPEC, 0, 0 }, // COM_A_UNSPEC
	{ NLA_U16, 0, 0 }, // COM_A_MOCK_ID
	{ NLA_UNSPEC, 0, 0 }, // COM_A_PAYLOAD
	{ NLA_U32, 0, 0 }, // COM_A_PAYLOAD_TYPE
	{ NLA_U16, 0, 0 }, // COM_A_RESULT
};

NLSocket::NLSocket() :
	cmds({{
		COM_C_SEND_DATA,
		&com_c_send_data_name[0],
		COM_A_MAX,
		NLSocket::parser,
		com_gnl_policy
	}}),
	ops({
		0, 0,
		&com_name[0],
		NULL,
		cmds.data(),
		COM_C_MAX,
		{}
	})
{
	register_family();
}

NLSocket::~NLSocket()
{
	genl_unregister_family(&ops);
	nl_close(socket_);
	nl_socket_free(socket_);
}

void NLSocket::receive() const
{
	nl_recvmsgs_default(socket_);
}

void NLSocket::send(mock_id id, void *payload, int payload_size, int payload_type)
{
	nl_msg *msg = NULL;
	struct genlmsghdr hdr = { COM_C_SEND_DATA, 0, 0};

	msg = nlmsg_alloc_simple(family, 0);
	nlmsg_append(msg, &hdr, sizeof(hdr), NLMSG_ALIGNTO);

	if (nla_put_u16(msg, COM_A_MOCK_ID, id)) {
		std::cerr << __func__ << " cannot put COM_A_MOCK_ID.\n";
	}

	if (nla_put_u32(msg, COM_A_PAYLOAD_TYPE, payload_type)) {
		std::cerr << __func__ << " cannot put COM_A_PAYLOAD_TYPE.\n";
	}
	if (payload != NULL) {
		if (nla_put(msg, COM_A_PAYLOAD, payload_size, payload)) {
			std::cerr << __func__ << " cannot put COM_A_PAYLOAD.\n";
		}
	}

	if ( nl_send_auto(socket_, msg) < 0) {
		std::cerr << __func__ << " nl_send_auto failed.\n";
	}

	nlmsg_free(msg);
}

int NLSocket::register_handler(mock_id id, handler_prototype handler)
{
	lock_t lock(handlers_mutex);
	return handlers.emplace(id, handler).second;
}

int NLSocket::unregister_handler(mock_id id)
{
	lock_t lock(handlers_mutex);
	return handlers.erase(id);
}

int NLSocket::parser(struct nl_cache_ops *, struct genl_cmd *, struct genl_info *info, void *arg)
{
	struct nlattr *attr;
	attr = nlmsg_find_attr(info->nlh, sizeof(struct genlmsghdr), COM_A_PAYLOAD_TYPE);
	int payload_type = nla_get_u32(attr);

	attr = nlmsg_find_attr(info->nlh, sizeof(struct genlmsghdr), COM_A_MOCK_ID);
	int mock_id = nla_get_u16(attr);

	attr = nlmsg_find_attr(info->nlh, sizeof(struct genlmsghdr), COM_A_PAYLOAD);
	void *payload = nla_data(attr);
	int payload_size = nla_len(attr);

	NLSocket *_this = static_cast<NLSocket*>(arg);
	lock_t lock(_this->handlers_mutex);
	auto handler_entry = _this->handlers.find(mock_id);
	if (handler_entry == _this->handlers.end()) {
		std::cerr << __func__ << " cannot find handler function for given mock id.\n";
		return -1;
	} else {
		handler_entry->second(payload, payload_size, payload_type);
	}

	return 0;
}

int NLSocket::register_family()
{
	int ret = -1;

	socket_ = nl_socket_alloc();
	if (socket_ == NULL) {
		std::cerr << __func__ << " nl_socket_alloc failed\n";
		return ret;
	}
	nl_socket_disable_seq_check(socket_);
	nl_socket_modify_cb(socket_, NL_CB_VALID, NL_CB_CUSTOM, genl_handle_msg, this);

	ret = genl_register_family(&ops);
	if (ret != 0) {
		std::cerr << __func__ << " genl_register_family failed\n";
		return ret;
	}

	ret = genl_connect(socket_);
	if (ret != 0) {
		std::cerr << __func__ << " genl_connect failed\n";
		return ret;
	}

	ret = genl_mngt_resolve(socket_);
	if (ret != 0) {
		std::cerr << __func__ << " genl_mngt_resolve failed\n";
		return ret;
	}

	family = ops.o_id;
	return ops.o_id;
}
