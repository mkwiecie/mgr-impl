/*
 * mock_controller.h
 *
 *  Created on: May 20, 2016
 *      Author: Maciej Kwiecień
 */

#ifndef MOCK_CONTROLLER_H
#define MOCK_CONTROLLER_H

#include <memory>
#include <thread>
#include <atomic>
#include "netlink_communication.h"

struct common_mock_data
{
    int mock_id;
    int register_msg_id;
    int unregister_msg_id;
};

class mock_controller
{
public:
    mock_controller(const common_mock_data &mock_info);
    virtual ~mock_controller();

    void connect();
    void disconnect();

protected:
    virtual void dispatcher(void *payload, int payload_size, int payload_type) = 0;
    void send(void *payload, int payload_size, int payload_type);
    template <typename T>
    const T* receive(void *payload, size_t payload_size);

private:
    void unregister_from_kernelspace();
    void register_in_kernelspace();
    void receiver();

    const common_mock_data mock_info;
    NLSocket nl_socket;
    std::atomic_bool should_receive;
    std::unique_ptr<std::thread> receiver_thread;
};

template <typename T>
const T* mock_controller::receive(void *payload, size_t payload_size)
{
    return (sizeof(T) == payload_size) ? reinterpret_cast<T*>(payload) : nullptr;
}

#endif /* MOCK_CONTROLLER_H */
