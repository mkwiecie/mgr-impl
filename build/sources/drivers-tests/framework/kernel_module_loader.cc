#include <iostream>
#include <fstream>
#include <algorithm>
#include <functional>
#include <fcntl.h>
#include "kernel_module_loader.h"

extern "C" int init_module(void *module_image, unsigned long len, const char *param_values);
extern "C" int delete_module(const char *name, int flags);

kernel_module_loader::kernel_module_loader() : modules_path(std::getenv("KERNEL_MODULES_PATH"))
{}

kernel_module_loader::kernel_module_loader(std::initializer_list<std::string> names) : kernel_module_loader()
{
	load(names);
}

kernel_module_loader::kernel_module_loader(const std::string& name) : kernel_module_loader({name})
{}

kernel_module_loader::~kernel_module_loader()
{
	std::for_each(modules.rbegin(), modules.rend(), std::bind(&kernel_module_loader::unload_image, this, std::placeholders::_1));
}

bool kernel_module_loader::load(const std::string& name)
{
	return load({name});
}

bool kernel_module_loader::load(std::initializer_list<std::string> names)
{
	bool state = true;
	for (const std::string& name : names) {
		if (add_unique_module(name) && load_image(name) != 0) {
			erase_module(name);
			std::cerr << "kernel_module_loader: Failed to init module " << name << "!" << std::endl;
			state = false;
		}
	}
	return state;
}

bool kernel_module_loader::unload(const std::string& name)
{
	return unload({name});
}

bool kernel_module_loader::unload(std::initializer_list<std::string> names)
{
	bool state = true;
	for (const std::string& name : names) {
		if (unload_image(name) == 0) {
			erase_module(name);
		} else {
			std::cerr << "kernel_module_loader: Failed to remove module " << name << "!" << std::endl;
			state = false;
		}
	}
	return state;
}

int kernel_module_loader::load_image(const std::string &name) const
{
	std::ifstream file(modules_path + "/" + name);
	if (!file) {
		return -1;
	}
	auto data = std::string(std::istreambuf_iterator<char>(file), std::istreambuf_iterator<char>());
	return init_module(&data[0], data.size(), "");
}

int kernel_module_loader::unload_image(const std::string &name) const
{
	std::cerr << "kernel_module_loader: Unloading module " << path_to_filename(name) << "..." << std::endl;
	return delete_module(path_to_filename(name).c_str(), O_NONBLOCK);
}

bool kernel_module_loader::add_unique_module(const std::string& name)
{
	if (std::find(modules.begin(), modules.end(), name) == modules.end()) {
		modules.push_back(name);
		return true;
	}
	return false;
}

void kernel_module_loader::erase_module(const std::string& name)
{
	auto module = std::find(modules.begin(), modules.end(), name);
	if (module != modules.end()) {
		modules.erase(module);
	}
}


std::string kernel_module_loader::path_to_filename(const std::string &path) const
{
	auto beg = path.find_last_of('/') + 1;
	auto end = path.find_last_of('.');
	std::string modname = path.substr(beg, end - beg);
	std::replace(modname.begin(), modname.end(), '-', '_');
	return modname;
}

