#ifndef KERNEL_MODULE_LOADER_H
#define KERNEL_MODULE_LOADER_H

#include <string>
#include <vector>
#include <initializer_list>

class kernel_module_loader
{
public:
	kernel_module_loader();
	kernel_module_loader(const std::string& name);
	kernel_module_loader(std::initializer_list<std::string> names);
	~kernel_module_loader();

	bool load(const std::string& name);
	bool load(std::initializer_list<std::string> names);
	bool unload(const std::string& name);
	bool unload(std::initializer_list<std::string> names);

private:
	int load_image(const std::string &name) const;
	int unload_image(const std::string &name) const;
	bool add_unique_module(const std::string& name);
	void erase_module(const std::string& name);
	std::string path_to_filename(const std::string &path) const;

	const std::string modules_path;
	std::vector<std::string> modules;
};

#endif // KERNEL_MODULE_LOADER_H
